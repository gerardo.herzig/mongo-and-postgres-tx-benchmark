# TX_py_bench

### Benchmark for MongoDB and PostgreSQL with transaction support

## Usage:
```
txbench.py [-h] [--host HOST] -D DURATION -t TEST -c
                  {mongo,mongodb,postgres,postgresql,pg} [-d DATABASE]

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Host to connect
  -D DURATION, --duration DURATION
                        Duration (in seconds) for the test to run
  -t TEST, --test TEST  Name of the test to run
  -c {mongo,mongodb,postgres,postgresql,pg}, --database-class {mongo,mongodb,postgres,postgresql,pg}
                        Type of test [mongo | postgres]
  -d DATABASE, --database DATABASE
                        Name of the database
```

`txbench` supports several (ideally equivalent) benchmarks for _MongoDB_ and for _PostgreSQL_. The code for the tests resides in the `mongo_tests.py` and `postgresql_tests.py` modules. Feel free to write your own tests and happy _benchmarking_!