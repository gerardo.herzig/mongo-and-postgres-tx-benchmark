import pymongo
import datetime


class MongoCollection(object):
    def __init__(self, database, name):
        self.name = name
        self.database = database

    def create(self):
        self.collection = pymongo.collection.Collection(database=self.database, name=self.name, create=False)

    def insert(self, value,session=None):
        self.collection.insert_one(value, session)

    def get(self, filter=None):
        return self.collection.find_one()
    
    def getAll(self):
        return self.collection.find()

    def updateOne(self, filter, newvalues,upsert=True):
        return self.collection.update_one(filter, newvalues,upsert)

    def drop(self):
        self.collection.drop()
    


if __name__ == '__main__':
    server_addr = 'localhost'
    connString = 'mongodb://%s:27100,%s:27101,%s:27102/?replicaSet=txntest&retryWrites=true' % (server_addr,server_addr,server_addr)
    client = pymongo.MongoClient(host=connString)
    session = client.start_session()
    db = client["DB"]
    seats_collection = MongoCollection(db,'audit')
    seats_collection.create()

    
    
    print (seats_collection.getAll())
    for x in seats_collection.getAll():
        print (x)
