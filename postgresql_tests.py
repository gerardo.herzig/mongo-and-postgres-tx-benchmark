import psycopg2
import random
import datetime
import time
import json
import threading


class TXWrapper(object):
    
  def __init__(self, func, *args, **kwargs):
    self._func = func
    self._args = args
    self._kwargs = kwargs
    return None

  def __call__(self):
    
    return self._func(*self._args, **self._kwargs)


def TransactionTest(client):
  conn = client.cursor()
  
  total = 0
  account_id = random.randint(1,5000)
  insert_accounts_template = "insert into accounts values ('{}', '{}') ON CONFLICT (account_id) DO UPDATE set balance = accounts.balance + {}"
  insert_audit_template = "insert into audit (msg) values ('{}')"

  movements = 10
  for m in range(movements):
    amount = random.randint(1, 100)
    total = total + amount
    conn.execute(insert_accounts_template.format(account_id,amount,amount))
  now = str(datetime.datetime.utcnow())
  msg = {"account_id": account_id, "total": total, "date": now}
  conn.execute(insert_audit_template.format(json.dumps(msg)))
  client.commit()
  return


def run(test_name, server_addr, test_duration,interval,max_threads):
    header = "interval;timestamp;number_of_tx,thread#"
    #print (header)
    connString = "dbname='benchplatform' user='postgres' host='%s' password='1234'" % (server_addr)
    
    client = psycopg2.connect(connString)

    startTime = time.time()
    theTest = TXWrapper(test_name,client)
    while time.time() < startTime + test_duration: ## global loop
      tx = 0
      t0 = time.time()
      while time.time() < t0 + interval:  # One second period
        theTest()
        tx = tx + 1
      print ("{},{},{},{},{}\r".format(interval,datetime.datetime.utcnow(), tx, threading.currentThread().getName(),max_threads))