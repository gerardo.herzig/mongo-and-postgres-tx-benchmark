#! /usr/bin/python3

import os
import argparse
import importlib
import threading

parser = argparse.ArgumentParser()
parser.add_argument("--host", help="Host to connect", default="localhost")
parser.add_argument("-D", "--duration", help="Duration (in seconds) for the test to run", type=int, required=True)
parser.add_argument("-t", "--test", required=True, help="Name of the test to run")
parser.add_argument("-c", '--database-class', required=True, help="Type of test [mongo | postgres]", choices=["mongo", "mongodb","postgres","postgresql","pg"])
parser.add_argument("-d", '--database', help="Name of the database")
parser.add_argument("-i", "--interval", default=1 ,help="Checkpoint interval, in seconds (default 1sec)", type=int)
parser.add_argument("-p", "--parallel", help="How many connections running in parallel", default=1, type=int)

args = parser.parse_args()

server_addr = args.host
PERIOD = args.duration
testName = args.test
database = args.database
parallelWorkers = args.parallel


if args.database_class in ["mongo", "mongodb"]:
    mod = importlib.import_module('mongo_tests')
elif args.database_class in ["pg", "postgres","postgresql"]:
    mod = importlib.import_module('postgresql_tests')

test = getattr(mod, testName)

threads = list()
step = 1
for i in range(parallelWorkers):
    t = threading.Thread(target=mod.run, args=(test,server_addr,args.duration,args.interval,parallelWorkers))
    threads.append(t)
    t.start()




