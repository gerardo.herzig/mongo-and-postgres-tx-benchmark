from mongo import MongoCollection

import random
import datetime
import pymongo
import time
import threading

class TXWrapper(object):
    
  def __init__(self, func, *args, **kwargs):
    self._func = func
    self._args = args
    self._kwargs = kwargs
    return None

  def __call__(self):
    
    return self._func(*self._args, **self._kwargs)



def TransactionTest(session, db):
  accounts_collection = MongoCollection(db, 'accounts')
  accounts_collection.create()
  audit_collection = MongoCollection(db,'audit')
  audit_collection.create()
  
  total = 0
  account_id = random.randint(1,5000)
  
  session.start_transaction()
  movements = 10
  for m in range(movements):
    amount = random.randint(1, 100)
    total = total + amount
    accounts_collection.updateOne({ "account_id" : account_id}, { "$inc" : { "count" : amount}}, upsert=True)
   
  audit_collection.insert({"account_id": account_id, "total": total, "date": datetime.datetime.utcnow()}, session=session)
  session.commit_transaction()
  return

def run(test_name, server_addr, test_duration,interval,max_threads):
    header = "interval;timestamp;number_of_tx"
    #print (header)
    connString = 'mongodb://%s:27017/?replicaSet=myreplica&retryWrites=true' % (server_addr)
    client = pymongo.MongoClient(host=connString)
    db = client["DB"]
    
    startTime = time.time()
    with client.start_session() as session:
      theTest = TXWrapper(test_name,session, db)
      while time.time() < startTime + test_duration: ## global loop
        tx = 0
        t0 = time.time()
        while time.time() < t0 + interval:  # One second period per default
          theTest()
          tx = tx + 1
        print ("{},{},{},{},{}\r".format(interval,datetime.datetime.utcnow(), tx, threading.currentThread().getName(),max_threads))

    
        